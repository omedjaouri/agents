#Simple pyglet application to create a grid of tiles of various shapes and sizes

import pyglet
import random
from grid import Grid
from cell import Cell

GRID_SIZE = 12
WINDOW_HEIGHT = 1024
WINDOW_WIDTH = 1024

#Create a basic window
window = pyglet.window.Window(width=WINDOW_WIDTH, height=WINDOW_HEIGHT)
keyboard = pyglet.window.key.KeyStateHandler()
window.push_handlers(keyboard)

#Start random seed
random.seed()

#Generate a grid to display in pyglet
grid = Grid(WINDOW_HEIGHT, WINDOW_WIDTH, grid_size=GRID_SIZE, grid_shape='square')

@window.event
def on_draw():
    #Clear the window and draw the grid
    window.clear()
    grid.draw()


#Run the application
pyglet.app.run()



