#Simple grid class which will keep track of all of it's neighbors

import pyglet


class Cell():

    #Initialization function
    def __init__(self, x, y, cell_size, cell_cost, sprite, cell_neighbors=[]):
        #Store the x,y coordinate
        self.x = x
        self.y = y
        self.cell_size = cell_size
        #Store the cell cost
        self.cell_cost = cell_cost
        #Save list of neighbor cells in the arrangement
        self.neighbors = cell_neighbors
        self.img = sprite

    def assign_neighbors(cell_neighbors):
        self.neighbors = cell_neighbors

    #Cell draw function which draws a cell to the screen at a given position using pyglet
    def draw(self):
        #Draw a rectangle using pyglet at the given location
        self.img.blit(self.x, self.y)

