#Grid class which handles keeping track of the environment grid

import pyglet
import numpy as np
import random
from cell import Cell

class Grid():

    def __init__(self, window_width, window_height, grid_size=4, grid_shape="square"):
        #I don't think I need to allow for custom sizes of cells for display
        self.cell_size = 64

        #Determine where to start the grid.
        total_grid_dim = self.cell_size * grid_size
        self.x_offset = int((window_width  - total_grid_dim) / 2)
        self.y_offset = int((window_height  - total_grid_dim) / 2)

        #Load each of the possible cell types
        self.terrain_type = {}
        self.terrain_type["base"] = pyglet.image.load("assets/base.png")
        self.terrain_keys = ["base"]
        #Need to instantiate a grid of specified size and shape
        if grid_shape == "square":
            self.grid = self.generate_square_grid(grid_size)
        else:
            self.grid = []


    #Generates a square grid of a given size made up of individual cells.
    def generate_square_grid(self, grid_size):
        grid_list = [] 
        #Iterate through each row in a grid_size x grid_size grid
        for i in range(grid_size):
            for j in range(grid_size):
                #Determine x,y location and cost
                x = self.cell_size * i + self.x_offset
                y = self.cell_size * j + self.y_offset
                cost = random.uniform(0, 1)
                terrain = self.terrain_type[random.choice(self.terrain_keys)]
                #Append a grid at the current location
                grid_list.append(Cell(x, y, self.cell_size, cost, terrain))

        #Assign neighbors based on the grid
        #for i in range(grid_size):
        #    for j in range(grid_size):
        
        #Return the created grid
        return grid_list

    #Draws the current grid configuration to the screen
    def draw(self):
        #Iterate through each of the cells and draw them
        for cell in self.grid:
            cell.draw()
